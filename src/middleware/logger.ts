import { Request, Response, NextFunction } from "express";

const ErrorLogger = (
	err: Error,
	req: Request,
	res: Response,
	next: NextFunction
) => {
	console.error(err.stack);
	next(err);
};

export default ErrorLogger;
