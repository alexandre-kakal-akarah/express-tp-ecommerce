type JwtPayload = {
	userData: {
		id: number;
		email: string;
		name: string;
		role: string;
	};
	iat: number;
	exp: number;
};

export default JwtPayload;
