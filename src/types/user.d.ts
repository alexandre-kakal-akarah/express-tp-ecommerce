// override the default express type for req.user
type tUser = {
	id: number;
	name?: string;
	email: string;
	role: "admin" | "customer" | "manager";
};

export default tUser;
