import { z } from "zod";

export const userSchemaPatch = z.object({
	email: z.string().email(),
	name: z.string().max(255).optional(),
});
