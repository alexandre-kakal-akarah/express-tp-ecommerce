import { z } from "zod";
import { userSchemaPatch } from "./userSchema";

const passwordRegex =
	/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]{10,}$/;

export const signUpSchema = userSchemaPatch.extend({
	password: z.string().refine((data) => passwordRegex.test(data), {
		message:
			"Le mot de passe doit contenir au moins 10 caractères, une lettre minuscule, une lettre majuscule, un chiffre et un caractère spécial.",
	}),
});
