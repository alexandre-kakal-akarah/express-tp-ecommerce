import { z } from "zod";

export const ordersProductsSchema = z.object({
	productId: z.number(),
	orderId: z.number(),
	quantity: z.number().min(1),
});

export const ordersProductsSchemaPatch = z.object({
	quantity: z.number().min(1),
});
