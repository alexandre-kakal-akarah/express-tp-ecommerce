import "dotenv/config";
import "./utils/passport";
import prisma from "./utils/database";
import express from "express";
import api from "./routes/api";
import rateLimit from "express-rate-limit";
import cors from "cors";
import ErrorLogger from "./middleware/logger";

const app = express();
const port = 3000;

async function main() {
	const limiter = rateLimit({
		windowMs: 15 * 60 * 1000, // 15 minutes
		max: 100, // Limite de 100 requêtes par fenêtre
		message:
			"Trop de requêtes depuis cette adresse IP, veuillez réessayer plus tard.",
	});
	app.use(limiter);

	const allowedOrigins = [
		"http://localhost:3000",
		"https://express-tp-ecommerce.onrender.com",
	];

	app.use(
		cors({
			origin: function (origin, callback) {
				// allow requests with no origin
				// (like mobile apps or curl requests)
				if (!origin) return callback(null, true);
				if (allowedOrigins.indexOf(origin) === -1) {
					const msg =
						"The CORS policy for this site does not allow access from the specified Origin.";
					return callback(new Error(msg), false);
				}
				return callback(null, true);
			},
			methods: "GET,PATCH,POST,DELETE",
			credentials: true,
			optionsSuccessStatus: 204,
		})
	);

	app.use(express.json());
	app.use(express.urlencoded({ extended: false }));
	app.use(ErrorLogger);
	app.use("/api", api);
}

main()
	.then(() => {
		prisma.$disconnect();
	})
	.catch(async (e) => {
		console.log(e);
		await prisma.$disconnect();
	});

app.listen(port, () => {
	console.log(`Server is listening on port ${port}`);
});
