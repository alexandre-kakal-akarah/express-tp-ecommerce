import passport from "passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import JwtPayload from "../types/jwtPayload";

const params = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: process.env.JWT_SECRET,
};

passport.use(
	new Strategy(params, (payload: JwtPayload, done) => {
		// console.log(payload, "jwt payload");
		if (payload) {
			return done(null, payload.userData);
		}
		return done(null, false);
	})
);
