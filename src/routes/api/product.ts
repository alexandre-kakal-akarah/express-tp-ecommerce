import express from "express";
import prisma from "../../utils/database";
import tUser from "../../types/user";
import passport from "passport";
import { z } from "zod";
import { productSchema } from "../../data/productSchema";

const router = express.Router();

router.get("/", async (req, res) => {
	try {
		const products = await prisma.product.findMany();
		if (!products) {
			return res.status(404).json({ error: "Products not found" });
		}
		res.json(products);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.get("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const product = await prisma.product.findUnique({
			where: {
				id: Number(id),
			},
		});
		if (!product) {
			return res.status(404).json({ error: "Product not found" });
		}
		res.json(product);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.post(
	"/",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			if (req.user) {
				const user = req.user as tUser;
				if (user.role !== "admin" && user.role !== "manager") {
					return res.status(403).json({ error: "Unauthorized" });
				}
			}
			const validatedProduct = productSchema.parse(req.body);
			const { name, description, price } = validatedProduct;
			const product = await prisma.product.create({
				data: {
					name,
					description,
					price,
				},
			});
			res.status(201).json(product);
		} catch (error) {
			if (error instanceof z.ZodError) {
				return res.status(400).send({ message: error.message });
			}
			res.status(500).json({ error });
		}
	}
);

router.patch(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			if (req.user) {
				const user = req.user as tUser;
				if (user.role !== "admin" && user.role !== "manager") {
					return res.status(403).json({ error: "Unauthorized" });
				}
			}
			const { id } = req.params;
			const validatedProduct = productSchema.parse(req.body);
			const { name, description, price } = validatedProduct;
			const productExists = await prisma.product.findUnique({
				where: {
					id: Number(id),
				},
			});

			if (!productExists) {
				return res.status(404).json({ error: "Product not found" });
			}

			const product = await prisma.product.update({
				where: {
					id: Number(id),
				},
				data: {
					name,
					description,
					price,
				},
			});
			res.json(product);
		} catch (error) {
			if (error instanceof z.ZodError) {
				return res.status(400).send({ message: error.message });
			}
			res.status(500).json({ error });
		}
	}
);

router.delete(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			if (req.user) {
				const user = req.user as tUser;
				if (user.role !== "admin" && user.role !== "manager") {
					return res.status(403).json({ error: "Unauthorized" });
				}
			}
			const { id } = req.params;
			const product = await prisma.product.delete({
				where: {
					id: Number(id),
				},
			});
			res.status(204).json(product);
		} catch (error) {
			res.status(500).json({ error });
		}
	}
);

export default router;
