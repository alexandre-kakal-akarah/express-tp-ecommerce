import express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import prisma from "../../utils/database";
import { signUpSchema } from "../../data/signUpSchema";
import { signInSchema } from "../../data/signInSchema";
import { z } from "zod";

const router = express.Router();

router.post("/signin", async (req, res) => {
	// validation des données
	const validatedSignIn = signInSchema.parse(req.body);
	const { email, password } = validatedSignIn;
	try {
		const user = await prisma.user.findUnique({
			where: {
				email,
			},
		});
		if (!user) {
			return res.status(401).json({ message: "Invalid mail or password" });
		}
		const isSamePassword = await bcrypt.compare(password, user.password);
		if (!isSamePassword) {
			return res.status(401).json({ message: "Invalid mail or password" });
		}

		const { password: _, createdAt, updatedAt, ...userData } = user;

		// on génère un token
		const token = jwt.sign({ userData }, process.env.JWT_SECRET as string, {
			expiresIn: "1h",
		});

		return res.status(200).send({ token });
	} catch (error) {
		if (error instanceof z.ZodError) {
			return res.status(400).send({ message: error.message });
		}
		console.log(error);
		return res.status(500).send({ message: "Internal server error" });
	}
});

router.post("/signup", async (req, res) => {
	try {
		const validatedSignUp = signUpSchema.parse(req.body);
		const { email, password, name } = validatedSignUp;
		const hashedPassword = await bcrypt.hash(password, 10);
		const user = await prisma.user.create({
			data: {
				email,
				password: hashedPassword,
				name,
			},
		});

		const token = jwt.sign({ user }, process.env.JWT_SECRET as string, {
			expiresIn: "1h",
		});
		return res.status(200).send({ token });
	} catch (error) {
		if (error instanceof z.ZodError) {
			return res.status(400).send({ message: error.message });
		}
		console.log(error);
		return res.status(500).send({ message: "Internal server error" });
	}
});

export default router;
