import express from "express";
import prisma from "../../utils/database";
import tUser from "../../types/user";
import { z } from "zod";
import {
	ordersProductsSchema,
	ordersProductsSchemaPatch,
} from "../../data/ordersProductsSchema";

const router = express.Router();

router.get("/", async (req, res) => {
	try {
		const user = req.user as tUser;
		if (user.role === "customer") {
			const orders = await prisma.ordersProducts.findMany({
				where: {
					order: {
						userId: user.id,
					},
				},
			});
			if (!orders) {
				return res.status(404).json({ error: "No orders found." });
			}
			return res.json(orders);
		}

		const orders = await prisma.ordersProducts.findMany();
		if (!orders) {
			return res.status(404).json({ error: "No orders found." });
		}
		res.json(orders);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.get("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const user = req.user as tUser;
		if (user.role === "customer") {
			const order = await prisma.ordersProducts.findUnique({
				where: {
					id: Number(id),
				},
				select: {
					order: {
						select: {
							userId: true,
						},
					},
				},
			});
			if (!order) {
				return res.status(404).json({ error: "order not found." });
			}
			if (order.order.userId !== user.id) {
				return res.status(403).json({ error: "Forbidden." });
			}
			return res.json(order);
		}

		const order = await prisma.ordersProducts.findUnique({
			where: {
				id: Number(id),
			},
		});
		if (!order) {
			return res.status(404).json({ error: "order not found." });
		}
		res.json(order);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.post("/", async (req, res) => {
	try {
		const validatedOrdersProducts = ordersProductsSchema.parse(req.body);
		const { productId, orderId, quantity } = validatedOrdersProducts;
		const user = req.user as tUser;
		if (user.role === "customer") {
			const order = await prisma.order.findUnique({
				where: {
					id: orderId,
				},
			});
			if (!order) {
				return res.status(404).json({ error: "order not found." });
			}
			if (order.userId !== user.id) {
				return res.status(403).json({ error: "Forbidden." });
			}
		}

		const ordersProducts = await prisma.ordersProducts.create({
			data: {
				productId,
				orderId,
				quantity,
			},
		});
		res.status(201).json(ordersProducts);
	} catch (error) {
		if (error instanceof z.ZodError) {
			return res.status(400).send({ message: error.message });
		}
		res.status(500).json({ error });
	}
});

router.patch("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const validatedOrdersProducts = ordersProductsSchemaPatch.parse(req.body);
		const { quantity } = validatedOrdersProducts;

		const user = req.user as tUser;
		if (user.role === "customer") {
			const order = await prisma.ordersProducts.findUnique({
				where: {
					id: Number(id),
				},
				select: {
					order: {
						select: {
							userId: true,
						},
					},
				},
			});
			if (!order) {
				return res.status(404).json({ error: "order not found." });
			}
			if (order.order.userId !== user.id) {
				return res.status(403).json({ error: "Forbidden." });
			}
		}

		const product = await prisma.ordersProducts.update({
			where: {
				id: Number(id),
			},
			data: {
				quantity,
			},
		});
		res.json(product);
	} catch (error) {
		if (error instanceof z.ZodError) {
			return res.status(400).send({ message: error.message });
		}
		res.status(500).json({ error });
	}
});

router.delete("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const user = req.user as tUser;
		if (user.role === "customer") {
			const order = await prisma.ordersProducts.findUnique({
				where: {
					id: Number(id),
				},
				select: {
					order: {
						select: {
							userId: true,
						},
					},
				},
			});
			if (!order) {
				return res.status(404).json({ error: "order not found." });
			}
			if (order.order.userId !== user.id) {
				return res.status(403).json({ error: "Forbidden." });
			}
		}

		const order = await prisma.ordersProducts.delete({
			where: {
				id: Number(id),
			},
		});
		res.status(204).json(order);
	} catch (error) {
		res.status(500).json({ error });
	}
});

export default router;
