import express from "express";
import prisma from "../../utils/database";
import passport from "passport";
import tUser from "../../types/user";
import { z } from "zod";
import { orderSchema } from "../../data/orderSchema";

const router = express.Router();

router.get(
	"/",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const user = req.user as tUser;
			if (user.role === "customer") {
				const orders = await prisma.order.findMany({
					where: {
						userId: user.id,
					},
				});
				if (!orders) {
					return res.status(404).json({ error: "No orders found." });
				}
				return res.json(orders);
			}

			// for managers and admins
			const orders = await prisma.order.findMany();
			if (!orders) {
				return res.status(404).json({ error: "No orders found." });
			}
			res.json(orders);
		} catch (error) {
			res.status(500).json({ error });
		}
	}
);

router.get(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const { id } = req.params;
			const user = req.user as tUser;
			if (user.role === "customer") {
				const order = await prisma.order.findUnique({
					where: {
						id: Number(id),
					},
				});
				if (!order) {
					return res.status(404).json({ error: "order not found." });
				}
				if (order.userId !== user.id) {
					return res.status(403).json({ error: "Forbidden." });
				}
				return res.json(order);
			}

			// for managers and admins
			const order = await prisma.order.findUnique({
				where: {
					id: Number(id),
				},
			});
			if (!order) {
				return res.status(404).json({ error: "order not found." });
			}
			res.json(order);
		} catch (error) {
			res.status(500).json({ error });
		}
	}
);

router.post(
	"/",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const user = req.user as tUser;
			if (user.role === "customer") {
				const order = await prisma.order.create({
					data: {
						userId: user.id,
					},
				});
				return res.status(201).json(order);
			}

			// for managers and admins
			const validatedOrder = orderSchema.parse(req.body);
			const { userId } = validatedOrder;
			const order = await prisma.order.create({
				data: {
					userId,
				},
			});
			res.status(201).json(order);
		} catch (error) {
			if (error instanceof z.ZodError) {
				return res.status(400).send({ message: error.message });
			}
			res.status(500).json({ error });
		}
	}
);

// router.patch("/:id", async (req, res) => {
// 	try {
// 		const { id } = req.params;
// 		const { status } = req.body;
// 		const product = await prisma.order.update({
// 			where: {
// 				id: Number(id),
// 			},
// 			data: {
// 				status,
// 			},
// 		});
// 		res.json(product);
// 	} catch (error) {
// 		res.status(500).json({ error });
// 	}
// });

router.delete(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const { id } = req.params;
			const user = req.user as tUser;

			if (user.role === "customer") {
				const order = await prisma.order.delete({
					where: {
						id: Number(id),
					},
				});
				if (order.userId !== user.id) {
					return res.status(403).json({ error: "Forbidden." });
				}
				return res.status(204).json(order);
			}

			// for managers and admins
			const order = await prisma.order.delete({
				where: {
					id: Number(id),
				},
			});
			res.status(204).json(order);
		} catch (error) {
			res.status(500).json({ error });
		}
	}
);

export default router;
