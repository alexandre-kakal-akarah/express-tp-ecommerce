import express from "express";
import prisma from "../../utils/database";
import bcrypt from "bcrypt";
import tUser from "../../types/user";
import passport from "passport";
import { z } from "zod";
import { userSchemaPatch } from "../../data/userSchema";
import { signUpSchema } from "../../data/signUpSchema";

const router = express.Router();

router.get("/", async (req, res) => {
	try {
		const users = await prisma.user.findMany();
		if (!users) {
			return res.status(404).json({ error: "Users not found" });
		}
		const userPayload = req.user as tUser | undefined;
		if (userPayload?.role === "customer") {
			const minifiedUsers = users.map((user) => {
				const { password, createdAt, updatedAt, role, ...minifiedUser } = user;
				return minifiedUser;
			});
			return res.json(minifiedUsers);
		}
		res.json(users);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.get("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const user = await prisma.user.findUnique({
			where: {
				id: Number(id),
			},
		});
		if (!user) {
			return res.status(404).json({ error: "User not found" });
		}
		const userPayload = req.user as tUser | undefined;
		if (userPayload?.role === "customer") {
			const { password, createdAt, updatedAt, role, ...minifiedUser } = user;
			return res.json(minifiedUser);
		}
		res.json(user);
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.post("/", async (req, res) => {
	try {
		const validatedSignUp = signUpSchema.parse(req.body);
		const { name, email, password } = validatedSignUp;
		const hashedPassword = await bcrypt.hash(password, 10);
		const user = await prisma.user.create({
			data: {
				name,
				email,
				password: hashedPassword,
			},
		});
		res.status(201).json(user);
	} catch (error) {
		if (error instanceof z.ZodError) {
			return res.status(400).json({ error: error.message });
		}
		res.status(500).json({ error });
	}
});

router.patch(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const { id } = req.params;
			const validatedUser = userSchemaPatch.parse(req.body);
			const { name, email } = validatedUser;
			const userPayload = req.user as tUser | undefined;
			if (userPayload?.role === "customer" && userPayload.id !== Number(id)) {
				return res.status(403).json({ error: "Unauthorized" });
			}

			const user = await prisma.user.update({
				where: {
					id: Number(id),
				},
				data: {
					name,
					email,
				},
			});
			res.json(user);
		} catch (error) {
			if (error instanceof z.ZodError) {
				return res.status(400).json({ error: error.message });
			}
			res.status(500).json({ error });
		}
	}
);

router.delete(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	async (req, res) => {
		try {
			const { id } = req.params;
			const userPayload = req.user as tUser | undefined;
			if (userPayload?.role === "customer" && userPayload.id !== Number(id)) {
				return res.status(403).json({ error: "Unauthorized" });
			}

			const user = await prisma.user.delete({
				where: {
					id: Number(id),
				},
			});
			res.status(204).json(user);
		} catch (error) {
			res.status(500).json({ error });
		}
	}
);

export default router;
