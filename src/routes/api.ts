import express from "express";
import productRoutes from "./api/product";
import userRoutes from "./api/user";
import orderRoutes from "./api/order";
import ordersProductsRoutes from "./api/ordersProducts";
import authRoutes from "./api/auth";
import passport from "passport";

const router = express.Router();

router.use("/product", productRoutes);
router.use("/user", userRoutes);
router.use("/order", orderRoutes);
router.use(
	"/orders-products",
	passport.authenticate("jwt", { session: false }),
	ordersProductsRoutes
);
router.use("/auth", authRoutes);

export default router;
